package org.kenewstar.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * zuul 网关服务,统一客户端请求入口
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/26
 */
@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@RestController
public class ZuulApp {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApp.class,args);
    }

    @GetMapping("/zuul")
    public String getZuul(){
        return "zuul";
    }
}
