package org.kenewstar.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


/**
 * 注册中心服务
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/26
 */
@EnableEurekaServer
@SpringBootApplication
public class RegisterApp {
    public static void main(String[] args) {
        try {
            SpringApplication.run(RegisterApp.class,args);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
