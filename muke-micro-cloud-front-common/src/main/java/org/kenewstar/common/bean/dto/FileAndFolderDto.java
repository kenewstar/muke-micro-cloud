package org.kenewstar.common.bean.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 文件和文件夹dto
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
public class FileAndFolderDto {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 类型,file/folder
     */
    private String type;
    /**
     * 文件或文件夹名称
     */
    private String name;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 父级id
     */
    private Long pid;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;
    /**
     * 文件类型
     */
    private String fileType;
    /**
     * 文件字节数
     */
    private Long fileByte;
    private String size;

    private Long actualId;

    public Long getActualId() {
        return actualId;
    }

    public void setActualId(Long actualId) {
        this.actualId = actualId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getFileByte() {
        return fileByte;
    }

    public void setFileByte(Long fileByte) {
        this.fileByte = fileByte;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
