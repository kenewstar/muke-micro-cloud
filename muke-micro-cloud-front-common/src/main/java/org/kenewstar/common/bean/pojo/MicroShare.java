package org.kenewstar.common.bean.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/10
 */
public class MicroShare implements Serializable {
    private Long id;
    private String shareName;
    private String shareUrl;
    private String shareCode;
    private String shareFileList;
    private Date shareTime;
    private Integer shareValidTime;
    private Integer userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShareName() {
        return shareName;
    }

    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getShareFileList() {
        return shareFileList;
    }

    public void setShareFileList(String shareFileList) {
        this.shareFileList = shareFileList;
    }

    public Date getShareTime() {
        return shareTime;
    }

    public void setShareTime(Date shareTime) {
        this.shareTime = shareTime;
    }

    public Integer getShareValidTime() {
        return shareValidTime;
    }

    public void setShareValidTime(Integer shareValidTime) {
        this.shareValidTime = shareValidTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
