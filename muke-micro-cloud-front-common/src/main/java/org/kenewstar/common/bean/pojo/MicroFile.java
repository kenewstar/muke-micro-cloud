package org.kenewstar.common.bean.pojo;

import java.util.Date;

/**
 * 用户文件类
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
public class MicroFile {

    private Long id;
    private String filename;
    private Long fileByte;
    private Long folderId;
    private Integer userId;
    private String deleted;
    private String diskFilename;
    private String fileType;
    private String type;
    private Date updateTime;
    private Long actualId;

    public Long getActualId() {
        return actualId;
    }

    public void setActualId(Long actualId) {
        this.actualId = actualId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFileByte() {
        return fileByte;
    }

    public void setFileByte(Long fileByte) {
        this.fileByte = fileByte;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getDiskFilename() {
        return diskFilename;
    }

    public void setDiskFilename(String diskFilename) {
        this.diskFilename = diskFilename;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
