package org.kenewstar.common.bean.pojo;

/**
 * 真实文件名信息[存储在磁盘中的文件]
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/19
 */
public class MicroActualFile {

    private Long id;

    private String diskFilename;

    public MicroActualFile() {
    }

    public MicroActualFile(String diskFilename) {
        this.diskFilename = diskFilename;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiskFilename() {
        return diskFilename;
    }

    public void setDiskFilename(String diskFilename) {
        this.diskFilename = diskFilename;
    }
}
