package org.kenewstar.common.annotation;

import java.lang.annotation.*;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/16
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NotNull {
}
