package org.kenewstar.common.exception.controller;

import org.kenewstar.common.utils.CommonResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
@RestControllerAdvice(basePackages = "org.kenewstar")
public class ExceptionController {
    /**
     * 全局异常处理返回结果
     * @return 统一返回对象
     */
    @ExceptionHandler(Exception.class)
    public CommonResult exception(){
        return CommonResult.failed();
    }

}
