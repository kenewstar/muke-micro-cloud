package org.kenewstar.common.utils;

import org.kenewstar.common.annotation.NotNull;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/16
 */
public class AssertUtil {
    /**
     * 参数不能为空
     * @param str 参数
     */
    public static void notNull(String str) {
        if (Objects.isNull(str)) {
            throw new IllegalArgumentException("参数不能为空");
        }
    }

    /**
     * 整型
     * @param integer 参数
     */
    public static void notNull(Integer integer) {
        if (Objects.isNull(integer)) {
            throw new IllegalArgumentException("参数不能为空");
        }
    }

    /**
     * Long
     * @param l 参数
     */
    public static void notNull(Long l) {
        if (Objects.isNull(l)) {
            throw new IllegalArgumentException("参数不能为空");
        }
    }

    /**
     * 对象类型
     * @param obj obj
     */
    public static void notNull(Object obj) {
        if (obj instanceof String) {
            notNull((String) obj);
        } else if (obj instanceof Long) {
            notNull((Long) obj);
        } else if (obj instanceof Integer) {
            notNull((Integer) obj);
        }
        // 获取class对象
        Class<?> objClass = obj.getClass();
        // 获取所有属性
        Field[] fields = objClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                NotNull notNull = field.getAnnotation(NotNull.class);
                if (Objects.nonNull(notNull) && Objects.isNull(field.get(obj))) {
                    throw new IllegalArgumentException(field.getName() + " 参数不能为空");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * 断言List集合
     * @param list 断言对象
     */
    public static void notNull(List<?> list) {
        if (Objects.isNull(list)) {
            throw new IllegalArgumentException("集合不能为null");
        }
        if (list.isEmpty()) {
            throw new IllegalArgumentException("集合不能为空");
        }
    }
}
