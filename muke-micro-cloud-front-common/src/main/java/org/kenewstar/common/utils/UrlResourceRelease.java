package org.kenewstar.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 放行url
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/3
 */
public class UrlResourceRelease {
    /**
     * 存放url
     */
    private static final List<String> URL = new ArrayList<>();
    static {
        URL.add("/user/userLogin");
        URL.add("/user/register");
        URL.add("/user/validate/url");
        URL.add("/user/reset/pwd");
        URL.add("/user/mail/send/code");
        URL.add("/user/mail/send/reset/pwd/url");
        URL.add("/file/share/query/url");
        URL.add("/file/share/extract/file");
        URL.add("/file/file/download/file");
        URL.add("/file/folder/query/share");
        URL.add("/file/folder/getFolder/share");
    }

    /**
     * 是否包含该url
     * @param url
     * @return true/false
     */
    public static boolean isContainUrl(String url){
        return URL.contains(url);
    }



}
