package org.kenewstar.common.utils;

/**
 * 统一返回结果
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
public class CommonResult {

    private Integer code;
    private String message;
    private Object data;

    /**
     * 统一返回结果,无数据
     * @return 结果对象
     */
    public static CommonResult success(){
        return success(null);
    }

    /**
     * 统一返回结果,有数据
     * @param data 数据对象
     * @return 结果对象
     */
    public static CommonResult success(Object data){
        return new CommonResult(200,"ok",data);
    }

    /**
     * 统一返回失败的结果
     * @return 结果对象
     */
    public static CommonResult failed(){
        return new CommonResult(400,"error",null);
    }

    /**
     * 自定义返回失败消息消息
     * @param message 消息
     * @return
     */
    public static CommonResult failed(String message){
        return new CommonResult(401,message,null);
    }


    public CommonResult() {
    }

    public CommonResult(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CommonResult{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
