package org.kenewstar.common.utils;

import cn.hutool.crypto.digest.MD5;
import com.alibaba.fastjson.JSONArray;
import org.kenewstar.common.constant.CommonConstant;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;


/**
 * 公共工具类
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
public class CommonUtil {
    private static final String RANDOM = "0123456789abcdefghijklmnopqrstuvwxyz";
    private static final int COUNT = 72;
    private static final String USER_PREFIX = "MMC";
    private static final String DATE_FORMAT = "yyyyMMddHHmmssSSS";


    /**
     * 生成url分享后缀
     * @return 后缀字符串
     */
    public static String generateShareUrlSuffix(){
        // 获取uuid
        String uuid = UUID.randomUUID().toString();
        // 分割
        String[] str = uuid.split("-");
        // 组装
        return str[0] + str[1] + str[2] + str[3] + str[4];
    }

    /**
     * 生成重置密码的唯一参数
     * @return 参数32位字符
     */
    public static String generateResetParam() {
        return generateShareUrlSuffix();
    }

    /**
     * 生成提取码
     * @param length 提取码长度
     * @return 提取码
     */
    public static String generateShareCode(int length){
        if (length <= 0 ) {
            length = 4;
        }
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<length;i++){
            sb.append(RANDOM.charAt(random.nextInt(RANDOM.length())));
        }
        return sb.toString();
    }

    /**
     * 将json数组转换为list
     * @param json json字符串
     * @param clz 转换类型
     * @param <T> t
     * @return List
     */
    public static <T> List<T> json2List(String json, Class<T> clz) {
        return JSONArray.parseArray(json, clz);
    }

    /**
     * 将字节数转换为可读大小
     * @param bytes 字节数
     * @return 大小
     */
    public static String byte2Size(long bytes) {
        if (bytes < CommonConstant.KB) {
            return bytes + "B";
        } else if (bytes < CommonConstant.MB) {
            return String.format("%.2f", (double)bytes / CommonConstant.KB) + "KB";
        } else if (bytes < CommonConstant.GB) {
            return String.format("%.2f", (double)bytes / CommonConstant.MB) + "MB";
        } else if (bytes < CommonConstant.TB) {
            return String.format("%.2f", (double)bytes / CommonConstant.GB) + "GB";
        } else {
            return "error";
        }
    }

    /**
     * md5加密
     * @param salt 盐
     * @param pwd 明文
     * @return 密文
     */
    public static String md5(String salt, String pwd) {
        MD5 md5 = new MD5(salt.getBytes());
        String md5Pwd = md5.digestHex(pwd.getBytes());
        for (int i = 0; i < COUNT; i ++) {
            md5Pwd = md5.digestHex(md5Pwd);
        }
        return md5Pwd;
    }

    /**
     * 校验邮箱格式
     * @param mail 邮箱地址
     * @return true/false
     */
    public static boolean validateMail(String mail) {
        if (Objects.isNull(mail)) {
            return false;
        }
//        String regex = "\\w+@\\w+(\\.\\w{2,3})*\\.\\w{2,3}";
        String regex = "^[A-Za-z0-9\\u4e00-\\u9fa5.]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
        return mail.matches(regex);
    }

    /**
     * 获取六位随机码
     * @return int
     */
    public static int generateCode() {
        return ThreadLocalRandom.current().nextInt(100000, 999999);
    }

    /**
     * 生成用户名
     * @return 用户名
     */
    public static String generateUsername() {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return USER_PREFIX + LocalDateTime.now().format(pattern);
    }

    /**
     * 获取缓存key
     * @param cachePrefix 缓存前缀
     * @param cacheName 缓存名称
     * @return 缓存key
     */
    public static String getCacheKey(String cachePrefix, String cacheName) {
        return cachePrefix + "-" + cacheName;
    }


}
