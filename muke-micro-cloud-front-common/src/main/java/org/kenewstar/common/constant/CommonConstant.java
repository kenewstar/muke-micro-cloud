package org.kenewstar.common.constant;

/**
 * 全局常量对象
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
public class CommonConstant {
    /**
     * 前端服务器地址
     */
    public static final String API_HOST = "http://127.0.0.1:8888";
    /**
     * session用户key
     */
    public static final String SESSION_USER = "user";
    /**
     * 标识用户的分享数据，存于session中
     */
    public static final String SESSION_SHARE = "share";
    public static final String FILE = "file";
    public static final String FOLDER = "folder";
    /**
     * 分享文件链接前缀
     */
    public static final String SHARE_URL_PREFIX = "/#/share/";

    public static final long KB = 1024L;
    public static final long MB = KB * KB;
    public static final long GB = MB * KB;
    public static final long TB = GB * KB;

    public static final String USERNAME = "kenewstar@163.com";
    public static final String SYSTEM_TITLE = "muke micro cloud";
    public static final String UTF_8 = "UTF-8";

    /**
     * 缓存前缀
     */
    public interface CachePrefix {
        String REGISTER = "register";
        String FORGET   = "forget";
        String UPDATE   = "update";
    }


}
