package org.kenewstar.user.service;

import org.kenewstar.common.bean.pojo.MicroUser;
import org.kenewstar.user.dto.ValidShareUrlReturnDto;

/**
 * 用户业务层
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
public interface UserService {
    /**
     * 用户登录
     * @param user 用户登录
     * @return 用户对象
     */
    MicroUser userLogin(MicroUser user);

    /**
     * 校验url是否存在
     * @param url url后缀
     * @return ValidShareUrlReturnDto
     */
    ValidShareUrlReturnDto validateShareUrl(String url);

    /**
     * 上传头像
     * @param avatar 头像编码
     * @param id 用户id
     * @return true/false
     */
    boolean uploadAvatar(String avatar,Integer id);

    /**
     * 获取头像
     * @param uid 用户id
     * @return base64
     */
    String getAvatar(Integer uid);

    /**
     * 根据邮箱获取用户信息
     * @param email 邮箱
     * @return user
     */
    MicroUser getUserByEmail(String email);

    /**
     * 用户注册
     * @param user 用户对象
     * @return row
     */
    int userRegister(MicroUser user);

    /**
     * 更新用户
     * @param user 用户对象
     * @return row
     */
    int updateUserById(MicroUser user);
}
