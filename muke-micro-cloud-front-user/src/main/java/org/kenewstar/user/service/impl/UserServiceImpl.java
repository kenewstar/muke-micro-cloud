package org.kenewstar.user.service.impl;

import org.kenewstar.common.bean.pojo.MicroUser;
import org.kenewstar.common.utils.DateUtil;
import org.kenewstar.user.dto.ValidShareUrlReturnDto;
import org.kenewstar.user.mapper.UserMapper;
import org.kenewstar.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public MicroUser userLogin(MicroUser user) {
        return userMapper.selectUserByUsername(user.getUsername());
    }

    @Override
    public ValidShareUrlReturnDto validateShareUrl(String url) {
        ValidShareUrlReturnDto returnDto = userMapper.selectShareByUrl(url);
        if (Objects.nonNull(returnDto)) {
            returnDto.setValid(DateUtil.compareCurrentDatePass(returnDto.getShareTime(), returnDto.getShareValidTime()));
        }
        return returnDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean uploadAvatar(String avatar, Integer id) {
        return userMapper.updateAvatarById(avatar, id) == 1;
    }

    @Override
    public String getAvatar(Integer uid) {
        return userMapper.selectAvatarById(uid);
    }

    @Override
    public MicroUser getUserByEmail(String email) {
        return userMapper.selectUserByEmail(email);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int userRegister(MicroUser user) {
        return userMapper.insertUser(user);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateUserById(MicroUser user) {
        return userMapper.updateUserById(user);
    }
}
