package org.kenewstar.user.thread;

import org.kenewstar.common.constant.CommonConstant;
import org.kenewstar.user.config.InternetAddressFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Date;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/19
 */
public class MailResetPwdRunnable implements Runnable {
    private final JavaMailSender javaMailSender;
    private String toMail;
    private String suffixUrl;

    public MailResetPwdRunnable(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    public void setToMail(String toMail) {
        this.toMail = toMail;
    }
    public void setSuffixUrl(String suffixUrl) {
        this.suffixUrl = suffixUrl;
    }

    @Override
    public void run() {
        // 重置密码 Url
        String resetUrl = CommonConstant.API_HOST + "/#/password/reset?p=" + suffixUrl +
                "&email=" + toMail;

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(InternetAddressFactory.getInstance());
        message.setBcc();
        message.setTo(toMail);
        message.setSubject("【muke micro cloud】重置密码通知");
        message.setSentDate(new Date());
        message.setText("您刚刚在【muke micro cloud】系统使用了找回密码功能。\n" +
                        "请在10分钟内点击下面的链接设置您的新密码: \n" +
                        "如果以下链接点击无效，请复制以下链接到浏览器地址栏直接打开。\n " +
                        resetUrl + " \n如果不是您操作，请忽略此邮件。");
        javaMailSender.send(message);
    }
}
