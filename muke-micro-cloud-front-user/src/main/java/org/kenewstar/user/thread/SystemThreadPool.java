package org.kenewstar.user.thread;

import io.netty.util.concurrent.DefaultThreadFactory;

import java.util.concurrent.*;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/15
 */
public class SystemThreadPool {

    private static final ExecutorService POOL
            = new ThreadPoolExecutor(1, 16, 60L,
            TimeUnit.SECONDS, new LinkedBlockingQueue<>(),
            new DefaultThreadFactory("system thread pool"));

    public static void submitSendMail(Runnable mail) {
        POOL.submit(mail);
    }
}
