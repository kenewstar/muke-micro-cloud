package org.kenewstar.user.thread;

import org.kenewstar.user.config.InternetAddressFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Date;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/19
 */
public class MailUpdatePwdRunnable implements Runnable {
    private final JavaMailSender javaMailSender;
    private String toMail;
    private int code;

    public MailUpdatePwdRunnable(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    public void setToMail(String toMail) {
        this.toMail = toMail;
    }
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public void run() {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(InternetAddressFactory.getInstance());
        message.setBcc();
        message.setTo(toMail);
        message.setSubject("【muke micro cloud】系统修改密码");
        message.setSentDate(new Date());
        message.setText("\t您的【muke micro cloud】账号进行密码修改操作," +
                "请确认为本人操作,验证码为 " + code +
                "有效期为10分钟");
        javaMailSender.send(message);

    }
}
