package org.kenewstar.user.thread;

import org.kenewstar.user.config.InternetAddressFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/15
 */
public class MailRegisterRunnable implements Runnable {

    private final JavaMailSender javaMailSender;
    private String toMail;
    private int code;

    public MailRegisterRunnable(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    public void setToMail(String toMail) {
        this.toMail = toMail;
    }
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public void run() {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(InternetAddressFactory.getInstance());
        message.setBcc();
        message.setTo(toMail);
        message.setSubject("【muke micro cloud】账号注册");
        message.setSentDate(new Date());
        message.setText("\t欢迎注册【muke micro cloud】账号，您的邮箱注册验证码为 " + code +
                "有效期为10分钟");
        javaMailSender.send(message);

    }
}
