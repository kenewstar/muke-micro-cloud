package org.kenewstar.user.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/12
 */
public class ValidShareUrlDto {
    @NotNull
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
