package org.kenewstar.user.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/12
 */
public class UploadAvatarDto {

    @NotNull
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
