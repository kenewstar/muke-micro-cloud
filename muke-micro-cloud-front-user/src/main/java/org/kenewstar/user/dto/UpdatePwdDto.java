package org.kenewstar.user.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/19
 */
public class UpdatePwdDto {
    @NotNull
    private String email;
    @NotNull
    private String code;
    @NotNull
    private String pwd;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
