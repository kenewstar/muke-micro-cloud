package org.kenewstar.user.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/20
 */
public class ResetPwdDto {

    @NotNull
    private String email;

    @NotNull
    private String param;

    @NotNull
    private String pwd;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
