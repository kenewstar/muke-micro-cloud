package org.kenewstar.user.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.kenewstar.common.bean.pojo.MicroShare;
import org.kenewstar.common.bean.pojo.MicroUser;
import org.kenewstar.user.dto.ValidShareUrlDto;
import org.kenewstar.user.dto.ValidShareUrlReturnDto;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
@Mapper
public interface UserMapper {
    /**
     * 通过用户名查询用户对象
     * @param username 用户名
     * @return 用户对象
     */
    MicroUser selectUserByUsername(String username);

    /**
     * 根据url查询分享数据
     * @param url url后缀
     * @return share对象
     */
    ValidShareUrlReturnDto selectShareByUrl(String url);

    /**
     * 更新头像
     * @param avatar
     * @param id
     * @return
     */
    int updateAvatarById(@Param("avatar") String avatar, @Param("id") Integer id);

    /**
     * 根据用户id获取头像
     * @param uid 用户id
     * @return base64
     */
    String selectAvatarById(@Param("id") Integer uid);

    /**
     * 根据邮箱查询用户信息
     * @param email 邮箱
     * @return user
     */
    MicroUser selectUserByEmail(@Param("email") String email);

    /**
     * 插入用户
     * @param user 用户对象
     * @return row
     */
    int insertUser(MicroUser user);

    /**
     * 更新
     * @param user
     * @return
     */
    int updateUserById(MicroUser user);
}
