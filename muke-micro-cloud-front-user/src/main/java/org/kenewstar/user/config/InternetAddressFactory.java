package org.kenewstar.user.config;

import org.kenewstar.common.constant.CommonConstant;

import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/15
 */
public class InternetAddressFactory {

    public static String getInstance() {
        try {
            return String.valueOf(new InternetAddress(CommonConstant.USERNAME, CommonConstant.SYSTEM_TITLE, CommonConstant.UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

}
