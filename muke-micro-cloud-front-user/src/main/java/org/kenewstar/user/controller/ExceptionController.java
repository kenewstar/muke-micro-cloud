package org.kenewstar.user.controller;

import org.kenewstar.common.utils.CommonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/16
 */
@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public CommonResult handleException(Throwable e) {
        e.printStackTrace();
        return CommonResult.failed(e.getMessage());
    }


}
