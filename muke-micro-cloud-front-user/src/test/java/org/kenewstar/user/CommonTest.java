package org.kenewstar.user;

import cn.hutool.crypto.SecureUtil;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.junit.Test;
import org.kenewstar.common.annotation.NotNull;
import org.kenewstar.common.utils.AssertUtil;
import org.kenewstar.common.utils.CommonUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/27
 */
public class CommonTest {

    @Test
    public void test(){
//        condition("op1").get();

    }

    /**
     * Predicate<T>
     * 接受一个输入参数，返回一个布尔值结果。
     * Supplier<T>
     * 无参数，返回一个结果。
     * 	Function<T,R>
     * 接受一个输入参数，返回一个结果。
     * Consumer<T>
     * 代表了接受一个输入参数并且无返回的操作
     * @param opName 操作名称
     */
    public Supplier<Object> condition(String opName){
        Map<String, Supplier<Object>> map = new HashMap<>();
        map.put("op1", ()->{
            System.out.println("sssssss");
            return false;
        });
        return map.get(opName);

    }

    @Test
    public void test1() {
        String s = CommonUtil.md5("kenewstar", "123456");
        System.out.println(s);
    }

    @Test
    public void test2() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        int code = random.nextInt(100000, 999999);
        int code2 = random.nextInt(100000, 999999);
        int code3 = random.nextInt(100000, 999999);
        int code4 = random.nextInt(100000, 999999);
        System.out.println(code);
        System.out.println(code2);
        System.out.println(code3);
        System.out.println(code4);

    }

    @Test
    public void test3() {
        String s = CommonUtil.generateUsername();
        System.out.println(s);
    }
    @Test
    public void test4() {
        AssertUtil.notNull(new A());
    }
    @Test
    public void test5() {
        boolean b = CommonUtil.validateMail("xinke.huang@hand-china.com");
        System.out.println(b);
        // 0373916727cdd927ce8aa416b142d208 123456

        String s = CommonUtil.md5("MMC20210416134355060", "kenewstar");
        System.out.println(s);

    }
}
class A{
    @NotNull
    private String a;
    private String b;
}