package org.kenewstar.file.controller;

import org.kenewstar.common.bean.dto.FileAndFolderDto;
import org.kenewstar.common.bean.pojo.MicroUser;
import org.kenewstar.common.constant.CommonConstant;
import org.kenewstar.common.utils.AssertUtil;
import org.kenewstar.common.utils.CommonResult;
import org.kenewstar.file.dto.*;
import org.kenewstar.file.service.FolderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 文件夹
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
@RestController
@CrossOrigin(value = CommonConstant.API_HOST,allowCredentials = "true")
@RequestMapping("/folder")
public class FolderController {

    @Resource
    private FolderService folderService;
    /**
     * 新增一个文件夹
     * @param folder 文件夹对象
     * @param request request
     * @return 结果
     */
    @PostMapping("/add")
    public CommonResult addFolder(@RequestBody AddFolderDto folder, HttpServletRequest request){
        AssertUtil.notNull(folder);
        HttpSession session = request.getSession();
        // 获取用户信息
        MicroUser user = (MicroUser) session.getAttribute(CommonConstant.SESSION_USER);
        folder.setUid(user.getId());
        // 新增文件夹并获取当前文件夹内容
        List<FileAndFolderDto> dtoList = folderService.addFolder(folder);
        // 返回列表内容
        return CommonResult.success(dtoList);
    }

    /**
     * 查询当前文件夹下的文件及文件夹
     * @param session session
     * @return 查询结果
     */
    @PostMapping("/query")
    public CommonResult queryAllFiles(@RequestBody QueryFolderDto dto, HttpSession session){
        // 获取用户信息
        MicroUser user = (MicroUser) session.getAttribute(CommonConstant.SESSION_USER);
        // 执行查询
        List<FileAndFolderDto> dtoList = folderService.queryAllFileAndFolder(user.getId(), dto.getPid());
        // 返回查询结果
        return CommonResult.success(dtoList);
    }

    /**
     * 查询分享文件的下一级文件列表
     * @param dto 参数
     * @return result
     */
    @PostMapping("/query/share")
    public CommonResult queryShareFiles(@RequestBody QueryShareDto dto) {
        AssertUtil.notNull(dto);
        List<FileAndFolderDto> dtoList = folderService.queryAllFileAndFolder(dto.getUid(), dto.getPid());
        return CommonResult.success(dtoList);
    }

    /**
     * 根据用户id查询已删除文件或文件夹
     * @param session 用户session
     * @return 查询结果
     */
    @PostMapping("/deleted/query")
    public CommonResult queryDeletedFiles(HttpSession session){
        // 获取用户信息
        MicroUser user = (MicroUser) session.getAttribute(CommonConstant.SESSION_USER);
        // 查询已删除文件或文件夹
        List<FileAndFolderDto> dtoList = folderService.queryDeletedFileAndFolder(user.getId());
        // 返回查询结果
        return CommonResult.success(dtoList);
    }

    /**
     * 根据id查询查询文件夹
     * @param dto id参数
     * @return 文件夹信息
     */
    @PostMapping("/getFolderById")
    public CommonResult getFolderById(@RequestBody FolderIdDto dto){
        AssertUtil.notNull(dto);
        // 返回文件夹数据
        return CommonResult.success(folderService.findFolderById(dto.getId()));
    }

    /**
     * 文件分享查询文件夹信息
     * @param dto 参数
     * @return result
     */
    @PostMapping("/getFolder/share")
    public CommonResult getShareFolderById(@RequestBody FolderIdDto dto){
        AssertUtil.notNull(dto);
        // 返回文件夹数据
        return CommonResult.success(folderService.findFolderById(dto.getId()));
    }

    /**
     * 删除文件或文件夹
     * @param folderList 需要删除的文件信息
     * @param session session对象
     * @return 执行结果
     */
    @PostMapping("/deleteFolders")
    public CommonResult deleteFolders(@RequestBody List<FileAndFolderDto> folderList,HttpSession session){
        AssertUtil.notNull(folderList);
        int i = folderService.batchDeleteFolderAndFile(folderList);
        return CommonResult.success(i);
    }

    /**
     * 恢复已删除的文件或文件夹
     * @param list 恢复的参数对象
     * @return 执行结果
     */
    @PostMapping("/refreshFolders")
    public CommonResult refreshFolders(@RequestBody List<FileAndFolderDto> list){
        AssertUtil.notNull(list);
        int i = folderService.batchRefreshFolderAndFile(list);
        return CommonResult.success(i);
    }

    /**
     * 彻底删除文件或文件夹
     * @param list
     * @return
     */
    @PostMapping("/complete/delete")
    public CommonResult completeDelete(@RequestBody List<FileAndFolderDto> list){
        AssertUtil.notNull(list);
        int i = folderService.completeDeleteFileAndFolder(list);
        return CommonResult.success(i);
    }

    /**
     * 查询当前文件夹下的子文件夹
     * @param dto 文件夹id
     * @param session session对象
     * @return result
     */
    @PostMapping("/query/folders")
    public CommonResult queryFolders(@RequestBody QueryFolderDto dto, HttpSession session) {
        // 获取用户信息
        MicroUser user = (MicroUser) session.getAttribute(CommonConstant.SESSION_USER);
        // 查询文件夹数据
        List<FolderData> folderDataList = folderService.selectFolderData(dto.getPid(), user.getId());
        // 返回数据
        return CommonResult.success(folderDataList);
    }

    /**
     * 保存移动的文件或文件夹
     * @param list 文件或文件夹列表
     * @param session session对象
     * @return result
     */
    @PostMapping("/save/move/files")
    public CommonResult saveMoveFilesAndFolders(@RequestBody List<FileAndFolderDto> list,
                                                Long folderId, HttpSession session) {
        // 保存
        return folderService.saveMoveFilesAndFolders(list, folderId);
    }
}
