package org.kenewstar.file.controller;

import org.kenewstar.common.utils.CommonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/1
 */
@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public CommonResult handleException(Throwable e){
        e.printStackTrace();
        return CommonResult.failed(e.getMessage());
    }

}
