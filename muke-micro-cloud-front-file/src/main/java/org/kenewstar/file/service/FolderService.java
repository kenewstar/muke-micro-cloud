package org.kenewstar.file.service;

import org.kenewstar.common.bean.dto.FileAndFolderDto;
import org.kenewstar.common.bean.pojo.MicroFolder;
import org.kenewstar.common.bean.pojo.MicroUser;
import org.kenewstar.common.utils.CommonResult;
import org.kenewstar.file.dto.AddFolderDto;
import org.kenewstar.file.dto.FileShareDto;
import org.kenewstar.file.dto.FolderData;

import java.util.List;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
public interface FolderService {
    /**
     * 新增文件夹操作
     * @param folder 文件夹
     * @return 文件和文件夹数据
     */
    List<FileAndFolderDto> addFolder(AddFolderDto folder);

    /**
     * 查询所有文件及文件夹
     * @param uid
     * @param pid
     * @return
     */
    List<FileAndFolderDto> queryAllFileAndFolder(Integer uid,Long pid);

    /**
     * 根据用户id查询已删除文件及文件夹
     * @param uid
     * @return
     */
    List<FileAndFolderDto> queryDeletedFileAndFolder(Integer uid);

    /**
     * 根据id查询文件夹
     * @param id
     * @return
     */
    MicroFolder findFolderById(Long id);

    /**
     * 批量删除文件夹和文件 [逻辑删除]
     * @param dtoList
     * @return
     */
    int batchDeleteFolderAndFile(List<FileAndFolderDto> dtoList);

    /**
     * 批量恢复文件和文件 [修改删除状态]
     * @param dtoList
     * @return
     */
    int batchRefreshFolderAndFile(List<FileAndFolderDto> dtoList);

    /**
     * 彻底删除文件和问件夹
     * @param dtoList
     * @return
     */
    int completeDeleteFileAndFolder(List<FileAndFolderDto> dtoList);

    /**
     * 根据分享的id集合查询分享的集具体数据
     * @param list
     * @return
     */
    List<FileAndFolderDto> selectFileAndFolderByIds(List<FileShareDto> list);

    /**
     * 查询文件夹下的子文件夹数据
     * @param folderId 文件夹id
     * @param uid 用户id
     * @return list
     */
    List<FolderData> selectFolderData(Long folderId, Integer uid);

    /**
     * 保存移动的文件或文件夹
     * @param list 文件或文件夹列表
     * @param folderId 文件夹id
     * @return result
     */
    CommonResult saveMoveFilesAndFolders(List<FileAndFolderDto> list, Long folderId);

    /**
     * 根据父文件夹id查询所有子文件夹
     * @param uid 用户id
     * @param pid 父文件夹id
     * @return list
     */
    List<MicroFolder> selectFoldersByPid(Integer uid, Long pid);




}
