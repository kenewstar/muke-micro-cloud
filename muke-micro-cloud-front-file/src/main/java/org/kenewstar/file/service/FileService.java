package org.kenewstar.file.service;

import org.kenewstar.common.bean.dto.FileAndFolderDto;
import org.kenewstar.common.bean.pojo.MicroFile;
import org.kenewstar.common.bean.pojo.MicroUser;
import org.kenewstar.file.dto.DiskFileData;
import org.kenewstar.file.dto.UpdateNameDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
public interface FileService {
    /**
     * 文件上传
     * @param fid
     * @param file
     * @param user
     * @return
     * @throws IOException
     */
    List<FileAndFolderDto> uploadFile(Long fid,MultipartFile file, MicroUser user) throws IOException;

    /**
     * 修改文件或文件夹名称
     * @param dto
     * @return
     */
    int updateName(UpdateNameDto dto);

    /**
     * 根据id查询文件信息
     * @param id
     * @return
     */
    MicroFile getFileById(Long id);

    /**
     * 搜索文件
     * @param id
     * @param filename
     * @return
     */
    List<FileAndFolderDto> searchFiles(Integer id,String filename);

    /**
     * 统计用户磁盘使用情况
     * @param user 用户对象
     * @return 使用情况数据
     */
    List<DiskFileData> totalUsedFileSize(MicroUser user);

    /**
     * 根据父文件夹id查询子文件夹
     * @param uid 用户id
     * @param pid 父文件夹id
     * @return list
     */
    List<MicroFile> selectFilesByPid(Integer uid, Long pid);

}
