package org.kenewstar.file.service;

import org.kenewstar.common.bean.pojo.MicroFile;
import org.kenewstar.common.bean.pojo.MicroFolder;
import org.kenewstar.common.bean.pojo.MicroShare;
import org.kenewstar.common.utils.CommonResult;
import org.kenewstar.file.dto.*;

import java.util.List;
import java.util.Map;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/10
 */
public interface ShareService {
    /**
     * 创建分享链接
     * @param dto 创造链接的参数
     * @param uid 用户id
     * @return 链接和提取码
     */
    ShareUrlAndCodeDto createShare(ShareFileParamDto dto, Integer uid);

    /**
     * 查询所有分享数据
     * @param uid 用户id
     * @return 分享列表
     */
    List<ShareFileUrlDto> queryAllShare(Integer uid);

    /**
     * 校验文件或文件夹未被删除的数量
     * @param list 删除参数
     * @return 影响行数
     */
    long validateFileDeleted(List<FileShareDto> list);

    /**
     * 批量删除分享文件数据
     * @param ids 删除参数
     * @return 影响行数
     */
    int deleteShare(List<Long> ids);

    /**
     * 提取文件
     * @param url 提取参数
     * @return 分享文件数据
     */
    MicroShare extractFile(String url);

    /**
     * 保存分享文件的文件以及文件夹
     * @param files
     * @param folders
     * @param filesMap
     * @param foldersMap
     * @param userId
     */
    CommonResult saveShareFilesAndFolders(List<MicroFile> files, List<MicroFolder> folders,
                                          Map<Long, List<MicroFile>> filesMap,
                                          Map<Long, List<MicroFolder>> foldersMap,
                                          Integer userId);
}
