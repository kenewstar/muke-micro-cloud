package org.kenewstar.file.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/10
 */
public class ShareFileParamDto {
    @NotNull
    private String fileListJson;
    @NotNull
    private String shareName;
    @NotNull
    private Integer validTime;

    public String getFileListJson() {
        return fileListJson;
    }

    public void setFileListJson(String fileListJson) {
        this.fileListJson = fileListJson;
    }

    public String getShareName() {
        return shareName;
    }

    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    public Integer getValidTime() {
        return validTime;
    }

    public void setValidTime(Integer validTime) {
        this.validTime = validTime;
    }
}
