package org.kenewstar.file.dto;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/4
 */
public class DownloadDto {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
