package org.kenewstar.file.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/14
 */
public class QueryShareDto {
    @NotNull
    private Long pid;
    @NotNull
    private Integer uid;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}
