package org.kenewstar.file.dto;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/1
 */
public class QueryFolderDto {
    private Long pid = -1L;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
}
