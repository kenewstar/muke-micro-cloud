package org.kenewstar.file.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/1
 */
public class FolderIdDto {
    @NotNull
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
