package org.kenewstar.file.dto;

/**
 * 分享链接及提取码
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/10
 */
public class ShareUrlAndCodeDto {

    private String url;
    private String code;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
