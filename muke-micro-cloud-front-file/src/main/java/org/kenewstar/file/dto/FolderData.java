package org.kenewstar.file.dto;

/**
 * isChildren:0 无子节点
 * isChildren:>1 有子节点
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/21
 */
public class FolderData {
    private Long id;
    private String label;
    private Long pid;
    private int isChildren;
    private boolean isLeaf;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public int getIsChildren() {
        return isChildren;
    }

    public void setIsChildren(int isChildren) {
        this.isChildren = isChildren;
    }

    public boolean getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }
}
