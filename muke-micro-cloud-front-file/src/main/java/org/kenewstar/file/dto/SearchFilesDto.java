package org.kenewstar.file.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/4
 */
public class SearchFilesDto {
    @NotNull
    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
