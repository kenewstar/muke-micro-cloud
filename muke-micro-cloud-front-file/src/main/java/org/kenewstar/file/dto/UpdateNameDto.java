package org.kenewstar.file.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * 修改文件或文件夹名称Dto
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/3
 */
public class UpdateNameDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
