package org.kenewstar.file.dto;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/11
 */
public class DiskFileData {
    private String name;
    private Long y;
    private String size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getY() {
        return y;
    }

    public void setY(Long y) {
        this.y = y;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
