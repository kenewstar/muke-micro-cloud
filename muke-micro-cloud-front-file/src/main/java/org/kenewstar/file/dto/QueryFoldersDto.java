package org.kenewstar.file.dto;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/21
 */
public class QueryFoldersDto {

    private Long folderId;

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }
}
