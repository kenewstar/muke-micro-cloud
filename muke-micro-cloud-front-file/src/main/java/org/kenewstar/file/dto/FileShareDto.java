package org.kenewstar.file.dto;

import java.io.Serializable;

/**
 * 分享文件列表
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/10
 */
public class FileShareDto implements Serializable {
    private Long id;
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
