package org.kenewstar.file.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.kenewstar.common.bean.dto.FileAndFolderDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/4/13
 */
public class ShareDataDto implements Serializable{
    private Long id;
    private String shareName;
    private String shareUrl;
    private String shareCode;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date shareTime;
    private List<FileAndFolderDto> fileList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShareName() {
        return shareName;
    }

    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public Date getShareTime() {
        return shareTime;
    }

    public void setShareTime(Date shareTime) {
        this.shareTime = shareTime;
    }

    public List<FileAndFolderDto> getFileList() {
        return fileList;
    }

    public void setFileList(List<FileAndFolderDto> fileList) {
        this.fileList = fileList;
    }
}
