package org.kenewstar.file.dto;

import org.kenewstar.common.annotation.NotNull;

/**
 * 新增文件夹的dto
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
public class AddFolderDto {
    @NotNull
    private String name;
    private Integer uid;
    private Long pid = -1L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    @Override
    public String toString() {
        return "AddFolderDto{" +
                "name='" + name + '\'' +
                ", uid=" + uid +
                ", pid=" + pid +
                '}';
    }
}
