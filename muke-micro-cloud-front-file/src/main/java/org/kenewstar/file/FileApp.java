package org.kenewstar.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/26
 */
@SpringBootApplication
@EnableEurekaClient
public class FileApp {
    public static void main(String[] args) {
        SpringApplication.run(FileApp.class,args);
    }
}
