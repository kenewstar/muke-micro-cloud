package org.kenewstar.file.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.kenewstar.common.bean.pojo.MicroActualFile;

/**
 * @author kenewstar
 * @version 1.0
 * @date 2021/5/19
 */
@Mapper
public interface ActualFileMapper {
    /**
     * 新增一个实际文件
     * @param file 文件
     * @return row
     */
    int insertActualFile(MicroActualFile file);

    /**
     * 根据id查询真实文件
     * @param id 主键
     * @return file
     */
    MicroActualFile selectActualFileById(Long id);



}
