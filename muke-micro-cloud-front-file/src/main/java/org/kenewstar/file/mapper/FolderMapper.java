package org.kenewstar.file.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.kenewstar.common.bean.dto.FileAndFolderDto;
import org.kenewstar.common.bean.pojo.MicroFolder;
import org.kenewstar.file.dto.AddFolderDto;
import org.kenewstar.file.dto.FileShareDto;
import org.kenewstar.file.dto.FolderData;
import org.kenewstar.file.dto.UpdateNameDto;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
@Mapper
public interface FolderMapper {
    /**
     * 插入文件夹
     * @param folderDto 文件夹
     * @return 1/0
     */
    int insertFolder(AddFolderDto folderDto);

    /**
     * 查询文件夹及文件
     * @param folderDto
     * @return
     */
    List<FileAndFolderDto> selectFileAndFolderById(AddFolderDto folderDto);

    /**
     * 根据用户id查询已删除文件及文件夹
     * @param uid
     * @return
     */
    List<FileAndFolderDto> selectDeletedFileAndFolderById(Integer uid);

    /**
     * 查询文件夹根据id
     * @param id
     * @return
     */
    MicroFolder selectFolderById(Long id);

    /**
     * 批量修改文件夹
     * @param dtoList
     * @param deleted N / Y
     * @return
     */
    int batchUpdateFolder(@Param("dtoList") List<FileAndFolderDto> dtoList,
                          @Param("deleted") String deleted);

    /**
     * 修改文件夹名
     * @param dto
     * @return
     */
    int updateFolderName(UpdateNameDto dto);

    /**
     * 根据父id查询子文件夹
     * @param pid 父id
     * @return
     */
    List<Long> selectFolderIdByPid(Long pid);

    /**
     * 批量删除文件夹
     * @param ids
     * @return
     */
    int batchDeleteFolder(@Param("ids") Set<Long> ids);

    /**
     * 查询未被删除的文件的数量
     * @param list
     * @return 数量
     */
    long selectCountNotDeleted(@Param("list") List<FileShareDto> list);

    /**
     * 根据id集合查询文件列表
     * @param list id集合
     * @return 文件列表
     */
    List<FileAndFolderDto> selectFoldersById(@Param("list") List<FileShareDto> list);

    /**
     * 查询文件夹数据
     * @param folderId 文件夹Id
     * @param uid 用户id
     * @return 文件夹数据
     */
    List<FolderData> selectFolderData(@Param("fid") Long folderId, @Param("uid") Integer uid);

    /**
     * 查询当前文件夹的子文件夹数量
     * @param pid 文件夹id
     * @return count
     */
    long selectFolderCountByPid(Long pid);

    /**
     * 根据id修改父id
     * @param id 主键id
     * @param pid 父id
     * @return row
     */
    int updateParentFolderIdById(@Param("id") Long id, @Param("pid") Long pid);

    /**
     * 根据父文件夹id查询子文件夹
     * @param uid 用户id
     * @param pid 父文件夹id
     * @return list
     */
    List<MicroFolder> selectFoldersByPid(@Param("uid") Integer uid, @Param("pid") Long pid);

    /**
     * 保存文件夹并返回id
     * @param folder 文件夹
     * @return row
     */
    int insertFolderAndGetKey(MicroFolder folder);
}
