package org.kenewstar.file.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.kenewstar.common.bean.dto.FileAndFolderDto;
import org.kenewstar.common.bean.pojo.MicroFile;
import org.kenewstar.common.bean.pojo.MicroFolder;
import org.kenewstar.file.dto.FileShareDto;
import org.kenewstar.file.dto.UpdateNameDto;

import java.util.List;
import java.util.Set;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/2/28
 */
@Mapper
public interface FileMapper {
    /**
     * 批量修改文件的删除状态
     * @param dtoList
     * @param deleted
     * @return
     */
    int batchUpdateFile(@Param("dtoList") List<FileAndFolderDto> dtoList,
                        @Param("deleted") String deleted);

    /**
     * 插入一条文件数据
     * @param file 文件对象
     * @return
     */
    int insertFile(MicroFile file);

    /**
     * 修改文件名
     * @param dto
     * @return
     */
    int updateFileName(UpdateNameDto dto);

    /**
     * 查询文件信息根据文件id
     * @param id
     * @return
     */
    MicroFile selectFileById(Long id);

    /**
     * 模糊查询文件名
     * @param id 用户id
     * @param filename 文件名[关键字]
     * @return
     */
    List<FileAndFolderDto> selectFilesByLike(@Param("id") Integer id,@Param("filename") String filename);

    /**
     * 根据文件夹id查询子文件id
     * @param fid
     * @return
     */
    List<Long> selectFileIdByFid(Long fid);

    /**
     * 批量删除文件
     * @param ids
     * @return
     */
    int batchDeleteFile(@Param("ids") Set<Long> ids);

    /**
     * 查询未被删除的文件数量
     * @param list
     * @return
     */
    long selectCountNotDeleted(@Param("list") List<FileShareDto> list);

    /**
     * 根据用户id统计文件字节总数
     * @param uid 用户id
     * @return 字节总数
     */
    Long selectSumFileByte(@Param("uid") Integer uid);

    /**
     * 根据id集合查询文件列表
     * @param list id集合
     * @return 文件列表
     */
    List<FileAndFolderDto> selectFilesById(@Param("list") List<FileShareDto> list);

    /**
     * 根据id修改文件的文件夹id
     * @param id 文件id
     * @param fid 文件夹id
     * @return
     */
    int updateFileFolderIdById(@Param("id") Long id, @Param("fid") Long fid);

    /**
     * 根据父文件夹id查询子文件
     * @param uid 用户id
     * @param pid 父文件夹id
     * @return list
     */
    List<MicroFile> selectFilesByPid(@Param("uid") Integer uid, @Param("pid") Long pid);

    /**
     * 批量插入文件数据
     * @param list 文件数据列表
     * @return rows
     */
    int batchInsertFiles(List<MicroFile> list);
}
