package org.kenewstar.file.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.kenewstar.common.bean.pojo.MicroShare;
import org.kenewstar.common.bean.pojo.MicroUser;

import java.util.List;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/10
 */
@Mapper
public interface ShareMapper {

    /**
     * 插入分享数据
     * @param share 插入对象
     * @return 1/0
     */
    int insertShare(MicroShare share);

    /**
     * 查询所有分享数据
     * @param uid 用户id
     * @return 分享数据列表
     */
    List<MicroShare> selectAllShare(@Param("uid")Integer uid);

    /**
     * 批量删除分享文件的数据
     * @param ids 删除参数
     * @return rows
     */
    int batchDeleteShare(@Param("ids") List<Long> ids);

    /**
     * 查询分享数据通过url后缀
     * @param url 参数
     * @return 分享数据对象
     */
    MicroShare selectShareByUrl(@Param("url") String url);
}
