package org.kenewstar.file.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author xinke.huang@hand-china.com
 * @version 1.0
 * @date 2021/3/1
 */
@Component
@ConfigurationProperties(prefix = "file.path")
public class PathProperties {
    private String storePath;

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }
}

